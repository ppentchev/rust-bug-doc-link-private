/*
 * Copyright (c) 2022  Peter Pentchev <roam@ringlet.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
//! Run a processing function or something.

use std::error::Error;

use expect_exit::ExpectedResult;

use crate::defs::Config;

/// The current processing state, mainly the count of a's encountered.
struct State {
    /// The number of letters 'a' encountered so far.
    value: usize,
}

/// Count the letters 'a' in the specified string.
///
/// # Errors
///
/// Will return an error if the number of letters 'a' exceeds the maximum
/// value of the `usize` return type.
#[inline]
pub fn process(cfg: &Config) -> Result<usize, Box<dyn Error>> {
    cfg.name
        .chars()
        .try_fold(
            State { value: 0 },
            |state, letter| -> Result<State, Box<dyn Error>> {
                if letter == 'a' {
                    Ok(State {
                        value: state.value.checked_add(1).expect_result_("too many a's")?,
                    })
                } else {
                    Ok(state)
                }
            },
        )
        .map(|state| state.value)
}

#[cfg(test)]
mod tests {
    use std::error::Error;

    use crate::defs::Config;

    #[test]
    fn test_count_none() -> Result<(), Box<dyn Error>> {
        assert_eq!(
            super::process(&Config {
                name: "hello".to_owned()
            })?,
            0
        );
        Ok(())
    }

    #[test]
    fn test_count_lots() -> Result<(), Box<dyn Error>> {
        assert_eq!(
            super::process(&Config {
                name: "abracadabra".to_owned()
            })?,
            5
        );
        Ok(())
    }

    #[test]
    fn test_clean_count_none() -> Result<(), Box<dyn Error>> {
        assert_eq!(
            super::process(
                &Config {
                    name: "hello".to_owned()
                }
                .clean()
            )?,
            0
        );
        Ok(())
    }

    #[test]
    fn test_clean_count_lots() -> Result<(), Box<dyn Error>> {
        assert_eq!(
            super::process(
                &Config {
                    name: "abracadabra".to_owned()
                }
                .clean()
            )?,
            0
        );
        Ok(())
    }
}
