/*
 * Copyright (c) 2022  Peter Pentchev <roam@ringlet.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
//! Common definitions for the library.

/// Runtime configuration for the library functions.
#[derive(Debug)]
#[non_exhaustive]
pub struct Config {
    /// A name to process or something.
    pub name: String,
}

/// Really clean the data up.
///
/// Remove all the letters 'a'.
/// This should cause the [`crate::process::process`] function to return zero, and
/// the [`crate::process::State`] structure's `value` field to never be incremented.
#[inline]
#[must_use]
fn do_clean(cfg: &Config) -> Config {
    Config {
        name: cfg.name.replace('a', ""),
    }
}

impl Config {
    /// Clean the data up, removing all a's from the name.
    #[inline]
    #[must_use]
    pub fn clean(&self) -> Self {
        do_clean(self)
    }
}

#[cfg(test)]
mod tests {
    use super::Config;

    #[test]
    fn test_replace_none() {
        assert_eq!(
            Config {
                name: "hello".to_owned()
            }
            .clean()
            .name,
            "hello"
        );
    }

    #[test]
    fn test_replace_many() {
        assert_eq!(
            Config {
                name: "abracadabra".to_owned()
            }
            .clean()
            .name,
            "brcdbr"
        );
    }
}
